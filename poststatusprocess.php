<!DOCTYPE html>
<html>
<!--Links the stylesheet style.css to poststatusprocess.php -->
<link rel="stylesheet" type="text/css" href="style.css">
<!--The head tag contains the title "Post Status" -->
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>Post Status</title>
</head>
<!--Body of web page which contains the divisions "banner", "left", "mid" -->
<body>
  <!-- Divisin banner contains the h1 heading "Status Posting System" -->
  <div id=banner>
    <h1>Satus Posting System</h1>
  </div>
  <!--Division left contains an un-ordered list with the list items containg links to the home and status form pages -->
  <div id=left>
    <ul>
      <li><a href="index.html">Return Home</a></li>
      <li><a href="poststatusform.php">Return to status form</a></li>
    </ul>
  </div>
  <!--Division mid contains php code for connecting and querying the database -->
  <div id=mid>
    <?php
    //Attempts to establish a connection to the datatbase rfn3218 with the correct credentials
    $connection = mysqli_connect("cmslamp14.aut.ac.nz", "rfn3218", "winter2017", "rfn3218");
    // If a conection cannot be established end the connection and display error message
    if (!$connection) {
      die("Connection failed:" . mysqli_connect_error());
    }
    //Checks to see if the table status exists in th database rfn3218
    $exists = mysqli_query($connection, "SELECT * FROM status");
    //if the table is not equal to false the fields from the form in poststatus.php are checked to see if they hold values 
    //If they do not hold values a specified error message is displayed.
    if ($exists !== false) {
      if (isset($_POST['statuscode'])) {
        $statuscode = $_POST['statuscode'];
        if (isset($_POST['status'])) {
          $status = $_POST['status'];
          if (isset($_POST['share'])) {
            $share = $_POST['share'];
            if (isset($_POST['date'])) {
              $date = $_POST['date'];
              if (isset($_POST['pertype'])) {
                $permission = $_POST['pertype'];
                $perm = implode(", ", $_POST["pertype"]);
                //queries the table status to check if the status code entered in the poststatus form already exits in the table.
                $selectsql = "SELECT * FROM status WHERE status_code = '$statuscode'";
                $result = mysqli_query($connection, $selectsql);
                //if rows greater than 0 are returned then the prgram lets the user know that the status code entered already exists in the table
                if (mysqli_num_rows($result) > 0) {
                  echo "Status code already exists in DB";
                  // if it doesn't exist already the values $statuscode, $status, $share, $date, $perm
                  // are inserted into the status table
                } else {
                  $sql = "INSERT INTO status (status_code, status, share, date, permission) VALUES ('$statuscode', '$status', '$share', '$date', '$perm')";
                  //if the values are successfully inserted into the table the page displays "Record created"
                  // else an error message is displayed on the page.
                  if ($connection->query($sql) === true) {
                    echo "Record created";
                  } else {
                    echo "Error: " . $sql . "<br>" . $connection->error;
                  }
                }
              } else {
                echo "Permission type not set";
              }
            } else {
              echo "Date not set";
            }
          } else {
            echo "Please tick a share option";
          }
        } else {
          echo "Status not set";
        }
      } else {
        echo "Status code not set";
      }
    } else {
      echo "Table doesn't exist";
    }
    //Database connection is closed
    $connection->close();
    ?>
  </div>
</body>
</html>