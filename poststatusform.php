<!DOCTYPE html>
<html>
<!--Links the stylesheet style.css to poststatusform.php -->
<link rel="stylesheet" type="text/css" href="style.css">
<!-- Heading contains the title "Post Status Form" -->
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>Post Status Form</title>
</head>
<!--Body of web page which contains the divisions "banner", "left", "mid" -->
<body>
  <!-- Division banner contains an h1 heading "Status Posting System"-->
  <div id=banner>
    <h1>Satus Posting System</h1>
  </div>
  <!-- Division left contains an un ordered list containg one list item which is a link to the home page -->
  <div id=left>
    <ul>
      <li><a href="index.html">Return to Home Page</a></li>
    </ul>
  </div>
  <!-- Division mid contains a form displayed in a table format this form asks for the details "Status Code", "Status",
   "Share", "Date" and "Permission", these displayed fields are to be validated to meet specified conditions before being sent
    to "poststatusprocess.php" using the post method, this form also contains a submit an reset button. -->
  <div id=mid>
    <form action="poststatusprocess.php" method="post">
      <table align="left">
        <tr>
          <td>Status Code (required): </td>
          <td><input type="text" name="statuscode" maxlength="5" pattern="[S]{1}[0-9]{4}" required></td>
        </tr>
        <tr>
          <td>Status (required): </td>
          <td><input type="text" name="status" pattern="[A-Za-z0-9,.!?\s]+" required></td>
        </tr>
        <tr>
          <td>Share: </td>
          <td>
            <input type="radio" name="share" value="public"> Public
            <input type="radio" name="share" value="friends"> Friends
            <input type="radio" name="share" value="me"> Only me
          </td>
        </tr>
        <tr>
          <td>Date: </td>
          <td><input type="date" name="date" value="<?php echo date('Y-m-d'); ?>"></td>
        </tr>
        <tr>
          <td>Permission Type: </td>
          <td>
            <input type="checkbox" name="pertype[]" value="like"> Allow Like
            <input type="checkbox" name="pertype[]" value="comment"> Allow Comment
            <input type="checkbox" name="pertype[]" value="share"> Allow Share
          </td>
          <td>
            <input type="submit" value="Post">
          </td>
          <td>
            <input type="reset">
          </td>
        </tr>
      </table>
    </form>
  </div>
</body>
</html>