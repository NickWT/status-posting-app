<!DOCTYPE html>
<html>
<!--Links the stylesheet style.css to searchstatusform.php -->
<link rel="stylesheet" type="text/css" href="style.css">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>Assignment One</title>
</head>
<!--Body of web page which contains the divisions "banner", "left", "mid" -->
<body>
  <div id="banner">
    <h1>Satus Posting System</h1>
  </div>
  <div id="left">
    <ul>
      <li><a href="index.html">Return Home</a></li>
      <li><a href="searchstatusform.html">Return to Search status form</a></li>
    </ul>
  </div>
  <div id="mid">
    <?php
    // assigns value for 'search_status' to variable $searchstatus
    $searchstatus = $_GET['search_status'];
    //establish connection to be rfn3218 using the the appropriate credentials
    $connection = mysqli_connect("cmslamp14.aut.ac.nz", "rfn3218", "winter2017", "rfn3218");
    //if a connection cannot be established stop attempting and display error message
    if (!$connection) {
      die("Connection failed:" . mysqli_connect_error());
    }
    // checks if table "status" exists in database "rfn3218"
    $exists = mysqli_query($connection, "SELECT * FROM status");
    //if the table status exists in the database rfn3218
    if ($exists !== false) {
      //Check if $seacrhstatus is not empty
      if (!empty($searchstatus)) {
        //Query the table for values that match $searchstatus
        $result = mysqli_query($connection, "SELECT * FROM status WHERE status LIKE '%{$searchstatus}%'");
        //if the query in $result returns more than 0 rows
        if (mysqli_num_rows($result) > 0) {
          echo "<table>";
          // output data by row
          while ($row = mysqli_fetch_assoc($result)) {
            echo "<tr><td> " . $row["status_code"] .
              "</td><td> " . $row["status"] .
              "</td><td> " . $row["share"] .
              "</td><td>: " . $row["date"] .
              "</td><td>: " . $row["permission"] .
              "</td><tr>";
          }
          echo "</table>";
        } else {
          echo "Could not find a match";
        }
      } else {
        echo "You did not input a string to search for<br>";
      }
    } else {
      echo "Table exists";
    }
    //close connection to the database rfn3218
    $connection->close();
    ?>
  </div>
</body>
</html>